# 无人机与地面机器人的协同环境感知与控制

## 1. 研究目标

机器人环境感知技术主要任务就是通过传感器，SLAM和数据融合等技术将机器人所处的周边环境描述出来，并为后续控制机器人提供支持（如导航等等）。而在实际应用中，我们大多都是使用单个机器人来进行环境的感知，由于机器人不可在空中俯视障碍物，只能一点一点身临其境去探索。这样的环境感知过于缓慢，实时性不高，而且不够精确。而对于单个无人机进行运作，虽说对于环境的感知较快且实时性较好，却无法进行精确控制，达不到预想的效果。

针对上述问题开展研究工作，研究将机器人与无人机有机融合，使用无人机来进行快速的环境感知并实时传输给机器人，而机器人根据接收到的地图信息，以及周围环境进行进一步修正，并进行自主控制：快速精确的到达目标点，进行物资运输或者紧急救援等活动。主要研究目标有：

对于无人机：
1. 使用摄像头来对地面进行较为精准的SLAM，生成高精密的二维、三维地图
2. 在生成地图的基础上，进行物体识别，从而为地面机器人的路径规划提供障碍物信息

对于地面机器人:
1. 地面机器人接收无人机信息，并根据自身周围环境通过传感器实时修正与精确定位
2. 跟据地图进行加权，判断可达与不可达位置，来进行路径规划与导航


## 2. 主要思路

研究思路和步骤：

1. 先通读一下主要的参考文献，建立对所研究问题的基本认识，了解基本的方法等。
	
2. 跑通无人机代码，让其可以进行SLAM或者SfM，生成地图。
	
3. 看一些通信的代码，建立一些对于通信的认识，并实现无人机与机器人实时通信。要求实时性高，传输质量好。

4. 将地图进行加权标记，标记出障碍物。并配置ros环境，并让机器人在此环境下实现智能导航，实时定位与修改地图。

## 3. 关键技术

突破如下的关键的技术点，方法就可基本实现：

1. 在无人机中跑通slam代码，生成地图的图像
2. 如何将机器人与无人机进行通讯，通过socket或者ssh？
3. 机器人将接收到的地图信息，转换成点云或者深度图像，分辨出斜坡断崖等障碍物，将其转化为加权地图。
4. 根据机器人自身传感器与接收到的地图，实现较为精准的定位。
5. 机器人根据实时地图情况进行修改：如遇到无人机无法探查到的山洞等等。并对运动规划指定策略，如：目标点不可达？探测到山洞？等等。

## 4. 研究计划

* Step 1: 跑通无人机slam代码，能够使用无人机建图。
* Step 2: 实现无人机与机器人通讯，实时传输地图等信息。 
* Step 3: 实现将地图信息转化为深度图像和加权代价地图。 
* Step 4: 制定运动策略，以及实时地图修改。
* Step 5: 整合代码,实验。

## 5. 参考资料

### 5.1 已有论文

* Bu, Shuhui, et al. "Map2DFusion: Real-time incremental UAV image mosaicing based on monocular slam." 2016 IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS). IEEE, 2016.
* Wang, Wei, et al. "TerrainFusion: Real-time Digital Surface Model Reconstruction based on Monocular SLAM." 2019 IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS). IEEE, 2019.
* Zhao, Yong, et al. "Real-Time Orthophoto Mosaicing on Mobile Devices for Sequential Aerial Images with Low Overlap." Remote Sensing 12.22 (2020): 3739.

### 5.2 论文查找

关键词： 空地协同，无人机slam


### 5.3 相关程序
* [Map2DFusion](https://gitee.com/pi-lab/Map2DFusion)
* [ORB_SLAM2](https://github.com/raulmur/ORB_SLAM2)
* [ORB_SLAM3](https://github.com/UZ-SLAMLab/ORB_SLAM3)
